package main

import (
	"caching-project/cache"
	"caching-project/cloudflare"
	"caching-project/routes"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print(".env file not found, skipping")
	}
}

func main() {
	cloudflare.Url = os.Getenv("CLOUDFLARE_URL")

	err := cache.InitializeCache()
	if err != nil {
		log.Fatal(err)
	}

	router := gin.Default()
	router.Use(cors.Default())

	router.GET("/block/:blockNumber", routes.GetBlockByNumber())
	router.GET("/block/:blockNumber/txs/:hash", routes.GetBlockByTxsHash())

	if err := router.Run(":8080"); err != nil {
		log.Fatal(err)
	}
}
