package routes

import (
	"caching-project/cache"
	"caching-project/cloudflare"
	"caching-project/models"
	"github.com/gin-gonic/gin"
)

func GetBlockByNumber() gin.HandlerFunc {
	return func(context *gin.Context) {
		blockNumber := context.Param("blockNumber")

		block, ok := cache.GetBlock(blockNumber)
		if !ok {
			block = cloudflare.GetBlock(blockNumber)
			cache.AddBlock(block)
		}

		context.JSON(200, gin.H{
			"block": block,
		})
	}
}

func GetBlockByTxsHash() gin.HandlerFunc {
	return func(context *gin.Context) {
		blockNumber := context.Param("blockNumber")
		txsHash := context.Param("hash")

		block, ok := cache.GetBlock(blockNumber)
		if !ok {
			block = cloudflare.GetBlock(blockNumber)
			cache.AddBlock(block)
		}

		var resultTransaction *models.Transaction
		for _, transaction := range block.Transactions {
			if txsHash == transaction.Hash || txsHash == transaction.TransactionIndex {
				resultTransaction = transaction
			}

		}
		context.JSON(200, gin.H{
			"transaction": resultTransaction,
		})
	}
}
