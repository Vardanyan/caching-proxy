module caching-project

go 1.14

require (
	github.com/flyaways/golang-lru v0.0.0-20190617091412-ec8b77fcae6c
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
	github.com/joho/godotenv v1.3.0
)
