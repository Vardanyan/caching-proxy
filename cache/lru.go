package cache

import (
	"caching-project/models"
	lru "github.com/flyaways/golang-lru"
)

var LruCache *lru.Cache

func InitializeCache() error {
	var err error
	LruCache, err = lru.New(20)
	return err
}

func GetBlock(blockNumber string) (*models.Block, bool) {
	if blockNumber != "latest" && LruCache.Contains(blockNumber) {
		block, ok := LruCache.Get(blockNumber)
		return block.(*models.Block), ok
	}
	return nil, false
}

func AddBlock(block *models.Block) {
	LruCache.Add(block.Number, block)
}
