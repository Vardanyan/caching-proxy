package models

type EthResponse struct {
	Id      int    `json:"id"`
	JsonRpc string `json:"jsonrpc"`
	Block   *Block `json:"result"`
}

type Block struct {
	Difficulty       string                 `json:"difficulty"`
	ExtraData        string                 `json:"extraData"`
	GasLimit         string                 `json:"gasLimit"`
	GasUsed          string                 `json:"gasUsed"`
	Hash             string                 `json:"hash"`
	LogsBloom        string                 `json:"logsBloom"`
	Miner            string                 `json:"miner"`
	MixHash          string                 `json:"mixHash"`
	Nonce            string                 `json:"nonce"`
	Number           string                 `json:"number"`
	ParentHash       string                 `json:"parentHash"`
	ReceiptsRoot     string                 `json:"receiptsRoot"`
	Sha3Uncles       string                 `json:"sha3Uncles"`
	Size             string                 `json:"size"`
	StateRoot        string                 `json:"stateRoot"`
	Timestamp        string                 `json:"timestamp"`
	TotalDifficulty  string                 `json:"totalDifficulty"`
	Transactions     []*Transaction         `json:"transactions"`
	TransactionsRoot string                 `json:"transactionsRoot"`
	Uncles           map[string]interface{} `json:"uncles"`
}

type Transaction struct {
	BlockHash        string `json:"blockHash"`
	From             string `json:"from"`
	To               string `json:"to"`
	TransactionIndex string `json:"transactionIndex"`
	Value            string `json:"value"`
	BlockNumber      string `json:"blockNumber"`
	GasPrice         string `json:"gasPrice"`
	Input            string `json:"input"`
	Nonce            string `json:"nonce"`
	R                string `json:"r"`
	Hash             string `json:"hash"`
	Gas              string `json:"gas"`
	Type             string `json:"type"`
	V                string `json:"v"`
	S                string `json:"s"`
}
