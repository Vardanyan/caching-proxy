package cloudflare

import (
	"bytes"
	"caching-project/models"
	"encoding/json"
	"net/http"
)

type BlockRequest struct {
	Id      int           `json:"id"`
	JsonRpc string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
}

var Url string

func GetBlock(blockNumber string) *models.Block {
	var requestBody = BlockRequest{
		Id:      1,
		JsonRpc: "2.0",
		Method:  "eth_getBlockByNumber",
		Params:  []interface{}{blockNumber, true},
	}

	jsonBytes, err := json.Marshal(requestBody)
	if err != nil {
		panic(err)
	}

	request, err := http.NewRequest("POST", Url, bytes.NewBuffer(jsonBytes))
	if err != nil {
		panic(err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}

	var ethResponse models.EthResponse
	err = json.NewDecoder(response.Body).Decode(&ethResponse)
	if err != nil {
	}

	return ethResponse.Block
}
