FROM golang:1.13 as builder

WORKDIR /app

COPY go.* ./
RUN go mod download

COPY .env ./
COPY . ./

RUN CGO_ENABLED=0 GOOS=linux go build -v -o caching-proxy
RUN chmod a+x caching-proxy

FROM alpine:3
RUN apk add --no-cache ca-certificates

COPY --from=builder /app/caching-proxy caching-proxy
COPY --from=builder /app/.env ./

CMD ./caching-proxy