## Ethereum Block Caching Proxy 

This service is for retrieving block and transaction details from Ethereum blockchain. The accessed block details are cached by their block number in LRU style.

### How to run?

You can either run the service locally or in a container.

🚀  Local: If you have `Go` installed on your local machine just go ahead and run the following in your terminal:

```bash
cd {path_to_project}/caching-proxy
go run app.go
```

🚀  In a container: In case you do not have `Go` installed but hopefully have `Docker`you can use this command:

```bash
docker run --name caching-proxy -p 8080:8080 -d nanevardanyan/caching-proxy
```

### API Documentation

##### Get details of the specified block

Retrieves block details by specified block number or the latest block based on the input.

```bash
GET http://localhost:8080/block/:blockNumber
```

`blockNumber` is the number of the block (ex. `0xbed2d1`). `latest` is also a valid input.



##### Get details of the specified transaction for the specified block

Retrieves transaction details by transaction hash or index of the given block.

```bash
GET http://localhost:8080/block/:blockNumber/txs/:hash
```

`blockNumber` is the number of the block (ex. `0xbed2d1`). `latest` is also a valid input.

`hash` is either the index(ex. `0x1`) or the hash(ex. `0x73e294546d31560926469cbdb3cb197bfaa26ac3a8433a39884176c8384c01c5`) of a transaction


